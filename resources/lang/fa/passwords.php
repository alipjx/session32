<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'رمز عبور شما تغییر کرد!',
    'sent' => 'ایمیل مربوط به تغییر رمز عبور برای شما ارسال شد.',
    'throttled' => 'لطفا قبل از تلاش مجدد کمی صبر کنید.',
    'token' => 'توکن مربوط به تغییرات نامعتبر است.',
    'user' => "کاربری با این آدرس ایمیل یافت نشد.",

];
